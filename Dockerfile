## Build
FROM rust:latest AS builder

RUN update-ca-certificates

# Create appuser
ENV USER=rapi
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

## Build dependencies
WORKDIR /app
RUN cargo init
COPY Cargo.toml Cargo.lock .
RUN cargo build --release
RUN rm src/*.rs

## Build application
COPY src src
RUN cargo build --release

## Final image
FROM gcr.io/distroless/cc-debian11

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /bin
COPY --from=builder /app/target/release/rapi .
COPY db db

USER "${USER}":"${USER}"

CMD ["/bin/rapi"]
