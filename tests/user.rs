use actix_http::Request;
use actix_web::{
    dev::{Service, ServiceResponse},
    http::{
        header::{ContentType, LOCATION},
        StatusCode,
    },
    test::{call_service, init_service, TestRequest},
    web::Data,
    App,
};
use rapi::{
    app::{self, Context},
    config,
};

#[actix_web::test]
async fn app_index_get_test() {
    let ctx = Data::new(app::context().await.unwrap());
    let app = init_service(
        App::new()
            .app_data(Data::clone(&ctx))
            .configure(config::services),
    )
    .await;
    let req = TestRequest::get()
        .uri("/api/users/")
        .app_data(ctx.clone())
        .to_request();

    let resp = call_service(&app, req).await;

    assert_eq!(resp.status(), StatusCode::OK);
}

#[actix_web::test]
async fn test_add_role_ok() {
    let ctx = Data::new(app::context().await.unwrap());
    let app = init_service(
        App::new()
            .app_data(Data::clone(&ctx))
            .configure(config::services),
    )
    .await;

    let resp = create_user(&ctx, &app).await;

    assert_eq!(resp.status(), StatusCode::CREATED);
    assert!(resp.headers().get(LOCATION).is_some());
    let user_uuid = location(&resp);

    let resp = create_role(&ctx, &app).await;

    assert_eq!(resp.status(), StatusCode::CREATED);
    assert!(resp.headers().get(LOCATION).is_some());
    let role_uuid = location(&resp);

    let req = TestRequest::put()
        .uri(format!("/api/users/{}/roles/{}", user_uuid, role_uuid).as_str())
        .app_data(ctx.clone())
        .to_request();
    let resp = call_service(&app, req).await;

    assert_eq!(resp.status(), StatusCode::OK);
}

#[actix_web::test]
async fn test_del_role_ok() {
    let ctx = Data::new(app::context().await.unwrap());
    let app = init_service(
        App::new()
            .app_data(Data::clone(&ctx))
            .configure(config::services),
    )
    .await;

    let resp = create_user(&ctx, &app).await;

    assert_eq!(resp.status(), StatusCode::CREATED);
    assert!(resp.headers().get(LOCATION).is_some());
    let user_uuid = location(&resp);

    let resp = create_role(&ctx, &app).await;

    assert_eq!(resp.status(), StatusCode::CREATED);
    assert!(resp.headers().get(LOCATION).is_some());
    let role_uuid = location(&resp);

    let req = TestRequest::put()
        .uri(format!("/api/users/{}/roles/{}", user_uuid, role_uuid).as_str())
        .app_data(ctx.clone())
        .to_request();
    let resp = call_service(&app, req).await;
    assert_eq!(resp.status(), StatusCode::OK);

    let req = TestRequest::delete()
        .uri(format!("/api/users/{}/roles/{}", user_uuid, role_uuid).as_str())
        .app_data(ctx.clone())
        .to_request();
    let resp = call_service(&app, req).await;
    assert_eq!(resp.status(), StatusCode::OK);
}

async fn create_user(
    ctx: &Data<Context>,
    app: impl Service<Request, Response = ServiceResponse, Error = actix_web::Error>,
) -> ServiceResponse {
    let req = TestRequest::post()
        .uri("/api/users/")
        .set_payload(r#"{"name":"test"}"#)
        .insert_header(ContentType::json())
        .app_data(ctx.clone())
        .to_request();

    let resp = call_service(&app, req).await;
    resp
}

async fn create_role(
    ctx: &Data<Context>,
    app: impl Service<Request, Response = ServiceResponse, Error = actix_web::Error>,
) -> ServiceResponse {
    let req = TestRequest::post()
        .uri("/api/roles/")
        .set_payload(r#"{"name":"test"}"#)
        .insert_header(ContentType::json())
        .app_data(ctx.clone())
        .to_request();

    let resp = call_service(&app, req).await;
    resp
}

fn location(resp: &ServiceResponse) -> &str {
    let (_, uuid) = resp
        .headers()
        .get(LOCATION)
        .and_then(|s| s.to_str().ok())
        .and_then(|s| s.rsplit_once('/'))
        .unwrap();
    uuid
}
