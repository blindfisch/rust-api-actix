# rust-api-actix

A small template for a REST-API implemented in Rust with actix for educational purposes.

## Getting started

```shell
cargo build
cargo run
```

## Test

```shell
cargo test
```

To test the api please import the Thunder Client Collection.

## Endpoints
all placeholders are the uuids of the specified resource

VERB    | Endpoint | Description
----    | -------- | ---
GET     | /health | app running?

### Users Resource
VERB    | Endpoint | Description
----    | -------- | ---
GET     | /api/users/ | returns a list of all users
GET     | /api/users/{user} | information of specified user
POST    | /api/users/ | creates a new user
PUT     | /api/users/ | changes a user
DELETE  | /api/users/{user} | deletes specified user

### Roles Resource
VERB    | Endpoint | Description
----    | -------- | ---
GET     | /api/roles/ | returns a list of all roles
GET     | /api/roles/{role} | information of specified role
POST    | /api/roles/ | creates a new role
PUT     | /api/roles/ | changes a role
DELETE  | /api/roles/{role} | deletes specified role

### Users-Roles
VERB    | Endpoint | Description
----    | -------- | ---
GET     | /api/users/{user}/roles | list of all roles the user is part of
PUT     | /api/users/{user}/roles/{role} | adds the role to the user
DELETE  | /api/users/{user}/roles/{role} | removes the role from the user
GET     | /api/roles/{role}/users | list of all users in this role ¹

¹ not implemented

## DB-Diagram
```mermaid
erDiagram
    User ||--o{ Member : "is part of"
    Role ||--o{ Member : has

    User {
        int    id PK
        blob   uuid
        string name
    }
    Role {
        int    id PK
        blob   uuid
        string name
    }
    Member {
        int id PK
        int user_id FK
        int role_id FK
    }

```
## Docker
```shell
podman build -t rapi .
```

```shell
podman run -p8080:8080 --rm --user rapi --name rapi --env-file .env localhost/rapi
```
