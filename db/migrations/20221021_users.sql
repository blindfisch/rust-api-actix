CREATE TABLE IF NOT EXISTS users
(
    id      INTEGER PRIMARY KEY NOT NULL,
    uuid    BLOB    UNIQUE      NOT NULL,
    name    TEXT                NOT NULL
);
CREATE INDEX idx_users_uuid ON users (uuid);

CREATE TABLE IF NOT EXISTS roles
(
    id      INTEGER PRIMARY KEY NOT NULL,
    uuid    BLOB    UNIQUE      NOT NULL,
    name    TEXT                NOT NULL
);
CREATE INDEX idx_roles_uuid ON roles (uuid);

CREATE TABLE IF NOT EXISTS members
(
    id      INTEGER PRIMARY KEY NOT NULL,
    user_id INTEGER             NOT NULL,
    role_id INTEGER             NOT NULL,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(role_id) REFERENCES roles(id) ON DELETE CASCADE,
    UNIQUE(user_id, role_id)
);
