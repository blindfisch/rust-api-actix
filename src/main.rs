use actix_web::{web, App, HttpResponse, HttpServer};

use rapi::{app, config};
use tracing_actix_web::TracingLogger;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    config::init_app();

    let context = app::context().await?;
    let data = web::Data::new(context);
    let srv = HttpServer::new(move || {
        App::new()
            .wrap(TracingLogger::default())
            .app_data(web::Data::clone(&data))
            .service(web::resource("/health").route(web::get().to(HttpResponse::Ok)))
            .configure(config::services)
    })
    .bind(("0.0.0.0", 8080))?
    .run();
    srv.await
}
