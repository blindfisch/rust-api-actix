use actix_web::web;

mod db;
mod handler;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/users")
            .service(
                web::resource("/")
                    .route(web::get().to(handler::index))
                    .route(web::post().to(handler::create))
                    .route(web::put().to(handler::update)),
            )
            .service(
                web::resource("/{id}")
                    .route(web::get().to(handler::read))
                    .route(web::delete().to(handler::delete)),
            )
            .service(web::resource("/{id}/roles/").route(web::get().to(handler::roles)))
            .service(
                web::resource("/{user_id}/roles/{role_id}")
                    .route(web::put().to(handler::add_role))
                    .route(web::delete().to(handler::del_role)),
            ),
    );
}
