use actix_web::{
    http::header::{self},
    web::Path,
    web::{Data, Json},
    HttpRequest, HttpResponse,
};

use uuid::Uuid;

use crate::app::{db::Record, handler::Error, Context};

use super::db::{self, Role};

pub(crate) async fn index(ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let roles = db::list_all(&ctx.pool).await?;

    Ok(HttpResponse::Ok().json(roles))
}

pub(crate) async fn create(role: Json<Role>, req: HttpRequest, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let role = db::create(&ctx.pool, &role).await?;

    Ok(HttpResponse::Created()
        .insert_header((header::LOCATION, format!("{}{}", req.path(), role.uuid)))
        .finish())
}

pub(crate) async fn read(path: Path<Uuid>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let uuid = path.into_inner();
    let role = db::read(&ctx.pool, &uuid).await?;

    Ok(HttpResponse::Ok().json(role))
}

pub(crate) async fn update(role: Json<Record<Role>>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let role = db::update(&ctx.pool, &role).await?;

    Ok(HttpResponse::Ok().json(role))
}

pub(crate) async fn delete(path: Path<Uuid>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let uuid = path.into_inner();
    if db::delete(&ctx.pool, &uuid).await? {
        Ok(HttpResponse::NoContent().finish())
    } else {
        Ok(HttpResponse::NotFound().finish())
    }
}

#[cfg(test)]
mod tests {

    use crate::app::context;

    use super::*;
    use actix_web::{body, http::StatusCode, test::TestRequest};
    use pretty_assertions::assert_eq;

    #[actix_web::test]
    async fn test_index_ok() {
        let _req = TestRequest::default().to_http_request();
        let ctx = Data::new(context().await.unwrap());

        let resp = index(ctx).await.unwrap();

        assert_eq!(resp.status(), StatusCode::OK);
    }

    #[actix_web::test]
    async fn test_create_ok() {
        let req = TestRequest::default().to_http_request();
        let ctx = Data::new(context().await.unwrap());
        let role = Role::default();

        let resp = create(Json(role), req, ctx).await.unwrap();

        assert_eq!(resp.status(), StatusCode::CREATED);
        assert!(resp.headers().get(header::LOCATION).is_some());
    }

    #[actix_web::test]
    async fn test_update_ok() {
        let ctx = context().await.unwrap();
        let r1 = _create_role(&ctx, "foo").await;

        let mut r2 = r1.clone();
        r2.data.name = String::from("bar");
        let resp = update(Json(r2), Data::new(ctx)).await.unwrap();

        assert_eq!(resp.status(), StatusCode::OK);
        let body = body::to_bytes(resp.into_body()).await.unwrap();
        let r3 = serde_json::from_slice::<Record<Role>>(&body).unwrap();
        assert_eq!(r3.uuid, r1.uuid);
        assert_ne!(r3.data.name, r1.data.name);
    }

    #[actix_web::test]
    async fn test_delete_ok() {
        let ctx = context().await.unwrap();
        let r1 = _create_role(&ctx, "foo").await;

        let resp = delete(r1.uuid.into(), Data::new(ctx)).await.unwrap();

        assert_eq!(resp.status(), StatusCode::NO_CONTENT);
    }

    #[actix_web::test]
    async fn test_delete_fail() {
        let ctx = context().await.unwrap();

        let resp = delete(Uuid::new_v4().into(), Data::new(ctx)).await.unwrap();

        assert_eq!(resp.status(), StatusCode::NOT_FOUND);
    }

    #[actix_web::test]
    async fn test_read_ok() {
        let ctx = context().await.unwrap();
        let r1 = _create_role(&ctx, "foo").await;

        let resp = read(r1.uuid.into(), Data::new(ctx)).await.unwrap();

        assert_eq!(resp.status(), StatusCode::OK);
    }

    #[actix_web::test]
    async fn test_read_fail() {
        let ctx = context().await.unwrap();

        let resp = read(Uuid::new_v4().into(), Data::new(ctx)).await;
        assert!(resp.is_err());
    }

    async fn _create_role(ctx: &Context, name: &str) -> Record<Role> {
        let role = Role {
            name: String::from(name),
        };
        db::create(&ctx.pool, &role).await.unwrap()
    }
}
