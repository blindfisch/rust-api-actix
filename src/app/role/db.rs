use futures::Stream;
use serde::{Deserialize, Serialize};
use sqlx::{query, Error, Row as SqlxRow};
use uuid::Uuid;

use crate::app::db::{Pool, Record, Row};

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
pub struct Role {
    pub name: String,
}

fn map_row(row: Row) -> Result<Record<Role>, Error> {
    let role = Role {
        name: row.try_get("name")?,
    };
    Record::try_from(&row, role)
}

pub(crate) async fn list_all(pool: &Pool) -> Result<Vec<Record<Role>>, Error> {
    query(r#"SELECT * FROM roles"#)
        .try_map(map_row)
        .fetch_all(pool)
        .await
}

#[allow(dead_code)]
pub(crate) fn list(pool: &Pool) -> impl Stream<Item = Result<Record<Role>, Error>> + '_ {
    query(r#"SELECT * FROM roles"#).try_map(map_row).fetch(pool)
}

/// reads role from db
pub(crate) async fn read(pool: &Pool, id: &Uuid) -> Result<Record<Role>, Error> {
    query(r#"SELECT * FROM roles WHERE uuid = ?1"#)
        .bind(id)
        .try_map(map_row)
        .fetch_one(pool)
        .await
}

pub(crate) async fn delete(pool: &Pool, id: &Uuid) -> Result<bool, Error> {
    let res = query(r#"DELETE FROM roles WHERE uuid = ?1"#)
        .bind(id)
        .execute(pool)
        .await?;
    Ok(res.rows_affected() == 1)
}

pub(crate) async fn update(pool: &Pool, role: &Record<Role>) -> Result<Record<Role>, Error> {
    query(r#"UPDATE roles SET name = ?2 WHERE uuid = ?1 RETURNING *"#)
        .bind(role.uuid)
        .bind(role.data.name.clone())
        .try_map(map_row)
        .fetch_one(pool)
        .await
}
pub(crate) async fn create(pool: &Pool, role: &Role) -> Result<Record<Role>, Error> {
    let uuid = Uuid::new_v4();

    query(r#"INSERT INTO roles (uuid, name ) VALUES ( ?1, ?2 ) RETURNING *"#)
        .bind(uuid)
        .bind(role.name.clone())
        .try_map(map_row)
        .fetch_one(pool)
        .await
}

pub(crate) async fn roles(pool: &Pool, user: &Uuid) -> Result<Vec<Record<Role>>, Error> {
    query(
        r#"SELECT * FROM roles r
             JOIN members m ON r.id = m.role_id
             WHERE m.user_id = (SELECT u.id from users u WHERE u.uuid = ?1)"#,
    )
    .bind(user)
    .try_map(map_row)
    .fetch_all(pool)
    .await
}
