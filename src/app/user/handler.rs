use actix_web::{
    http::header::{self},
    web::Path,
    web::{Data, Json},
    HttpRequest, HttpResponse,
};

use uuid::Uuid;

use crate::app::{self, db::Record, handler::Error, Context};

use super::db::{self, User};

/// Returns the list of users.
///
/// 200 -> list
pub(crate) async fn index(ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let users = db::list_all(&ctx.pool).await?;

    Ok(HttpResponse::Ok().json(users))
}

pub(crate) async fn create(user: Json<User>, req: HttpRequest, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let user = db::create(&ctx.pool, &user).await?;

    Ok(HttpResponse::Created()
        .insert_header((header::LOCATION, format!("{}{}", req.path(), user.uuid)))
        .finish())
}

pub(crate) async fn read(path: Path<Uuid>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let uuid = path.into_inner();
    let user = db::read(&ctx.pool, &uuid).await?;

    Ok(HttpResponse::Ok().json(user))
}

pub(crate) async fn update(user: Json<Record<User>>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let user = db::update(&ctx.pool, &user).await?;

    Ok(HttpResponse::Ok().json(user))
}

pub(crate) async fn delete(path: Path<Uuid>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let uuid = path.into_inner();
    if db::delete(&ctx.pool, &uuid).await? {
        Ok(HttpResponse::NoContent().finish())
    } else {
        Ok(HttpResponse::NotFound().finish())
    }
}

pub(crate) async fn roles(path: Path<Uuid>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let uuid = path.into_inner();
    let roles = app::role::db::roles(&ctx.pool, &uuid).await?;
    Ok(HttpResponse::Ok().json(roles))
}

pub(crate) async fn add_role(path: Path<(Uuid, Uuid)>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let path = path.into_inner();
    if app::member::db::create(&ctx.pool, &path.0, &path.1).await? {
        Ok(HttpResponse::Ok().finish())
    } else {
        Ok(HttpResponse::BadRequest().finish())
    }
}

pub(crate) async fn del_role(path: Path<(Uuid, Uuid)>, ctx: Data<Context>) -> Result<HttpResponse, Error> {
    let path = path.into_inner();

    if app::member::db::delete(&ctx.pool, &path.0, &path.1).await? {
        Ok(HttpResponse::Ok().finish())
    } else {
        Ok(HttpResponse::NotFound().finish())
    }
}
#[cfg(test)]
mod tests {

    use crate::app::context;

    use super::*;
    use actix_web::{body, http::StatusCode, test::TestRequest};
    use pretty_assertions::assert_eq;

    #[actix_web::test]
    async fn test_index_ok() {
        let _req = TestRequest::default().to_http_request();
        let ctx = Data::new(context().await.unwrap());

        let resp = index(ctx).await.unwrap();

        assert_eq!(resp.status(), StatusCode::OK);
    }

    #[actix_web::test]
    async fn test_create_ok() {
        let req = TestRequest::default().to_http_request();
        let ctx = Data::new(context().await.unwrap());
        let user = User::default();

        let resp = create(Json(user), req, ctx).await.unwrap();

        assert_eq!(resp.status(), StatusCode::CREATED);
        assert!(resp.headers().get(header::LOCATION).is_some());
    }

    #[actix_web::test]
    async fn test_update_ok() {
        let ctx = context().await.unwrap();
        let u1 = _create_user(&ctx, "foo").await;

        let mut u2 = u1.clone();
        u2.data.name = String::from("bar");
        let resp = update(Json(u2), Data::new(ctx)).await.unwrap();

        assert_eq!(resp.status(), StatusCode::OK);
        let body = body::to_bytes(resp.into_body()).await.unwrap();
        let u3 = serde_json::from_slice::<Record<User>>(&body).unwrap();
        assert_eq!(u3.uuid, u1.uuid);
        assert_ne!(u3.data.name, u1.data.name);
    }

    async fn _create_user(ctx: &Context, name: &str) -> Record<User> {
        let user = User {
            name: String::from(name),
        };
        db::create(&ctx.pool, &user).await.unwrap()
    }
}
