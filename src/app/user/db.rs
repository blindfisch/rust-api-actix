use futures::Stream;
use serde::{Deserialize, Serialize};
use sqlx::{query, Error, Row as SqlxRow};
use uuid::Uuid;

use crate::app::db::{Pool, Record, Row};

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
pub struct User {
    pub name: String,
}

fn map_row(row: Row) -> Result<Record<User>, Error> {
    let user = User {
        name: row.try_get("name")?,
    };
    Record::try_from(&row, user)
}

pub(crate) async fn list_all(pool: &Pool) -> Result<Vec<Record<User>>, Error> {
    query(r#"SELECT * FROM users"#)
        .try_map(map_row)
        .fetch_all(pool)
        .await
}

#[allow(dead_code)]
pub(crate) fn list(pool: &Pool) -> impl Stream<Item = Result<Record<User>, Error>> + '_ {
    query(r#"SELECT * FROM users"#).try_map(map_row).fetch(pool)
}

/// reads user from db
pub(crate) async fn read(pool: &Pool, id: &Uuid) -> Result<Record<User>, Error> {
    query(r#"SELECT * FROM users WHERE uuid = ?1"#)
        .bind(id)
        .try_map(map_row)
        .fetch_one(pool)
        .await
}

pub(crate) async fn delete(pool: &Pool, id: &Uuid) -> Result<bool, Error> {
    let res = query(r#"DELETE FROM users WHERE uuid = ?1"#)
        .bind(id)
        .execute(pool)
        .await?;
    Ok(res.rows_affected() == 1)
}

pub(crate) async fn update(pool: &Pool, user: &Record<User>) -> Result<Record<User>, Error> {
    query(r#"UPDATE users SET name = ?2 WHERE uuid = ?1 RETURNING *"#)
        .bind(user.uuid)
        .bind(user.data.name.clone())
        .try_map(map_row)
        .fetch_one(pool)
        .await
}
pub(crate) async fn create(pool: &Pool, user: &User) -> Result<Record<User>, Error> {
    let uuid = Uuid::new_v4();

    query(r#"INSERT INTO users (uuid, name ) VALUES ( ?1, ?2 ) RETURNING *"#)
        .bind(uuid)
        .bind(user.name.clone())
        .try_map(map_row)
        .fetch_one(pool)
        .await
}
