use actix_web::web;

pub mod db;
mod handler;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/roles")
            .service(
                web::resource("/")
                    .route(web::get().to(handler::index))
                    .route(web::post().to(handler::create))
                    .route(web::put().to(handler::update)),
            )
            .service(
                web::resource("/{id}")
                    .route(web::get().to(handler::read))
                    .route(web::delete().to(handler::delete)),
            ),
    );
}
