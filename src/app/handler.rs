use actix_web::{http::StatusCode, ResponseError};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("DB: {0}")]
    SqlError(#[from] sqlx::Error),
}
impl ResponseError for Error {
    fn status_code(&self) -> StatusCode {
        match *self {
            Error::SqlError(sqlx::Error::RowNotFound) => StatusCode::NOT_FOUND,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
#[allow(dead_code)]
mod guard {
    use actix_web::{
        guard::GuardContext,
        http::header::{self, ContentType},
    };

    fn json(ctx: &GuardContext<'_>) -> bool {
        match ctx.header::<header::ContentType>() {
            Some(hdr) => hdr.subtype() == ContentType::json().subtype(),
            None => false,
        }
    }
}
