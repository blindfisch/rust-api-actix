use ::serde::{Deserialize, Serialize};
use sqlx::{Error, Row as SqlxRow, Sqlite};
use uuid::Uuid;

pub type Row = sqlx::sqlite::SqliteRow;
pub type Pool = sqlx::Pool<Sqlite>;

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
pub struct Record<D> {
    pub uuid: Uuid,
    #[serde(flatten)]
    pub data: D,
}

impl<D> Record<D> {
    pub fn try_from(row: &Row, data: D) -> Result<Self, Error> {
        let uuid = row.try_get("uuid")?;
        Ok(Record { uuid, data })
    }
}
