use sqlx::{query, Error};
use uuid::Uuid;

use crate::app::db::Pool;

pub(crate) async fn create(pool: &Pool, user_uuid: &Uuid, role_uuid: &Uuid) -> Result<bool, Error> {
    let res = query(
        r#"INSERT INTO members (user_id, role_id)
            SELECT u.id, r.id
            FROM users u CROSS JOIN roles r
            WHERE u.uuid = ?1
            AND   r.uuid = ?2"#,
    )
    .bind(user_uuid)
    .bind(role_uuid)
    .execute(pool)
    .await?;
    Ok(res.rows_affected() == 1)
}

pub(crate) async fn delete(pool: &Pool, user_uuid: &Uuid, role_uuid: &Uuid) -> Result<bool, Error> {
    let res = query(
        r#"DELETE FROM members
		   WHERE id IN (
			 SELECT m.id from members m
			 INNER JOIN users u ON u.id = m.user_id
			 INNER JOIN roles r ON r.id = m.role_id
			 WHERE u.uuid = ?1
			 AND   r.uuid = ?2
		   )"#,
    )
    .bind(user_uuid)
    .bind(role_uuid)
    .execute(pool)
    .await?;
    Ok(res.rows_affected() == 1)
}
