use actix_web::web;
use dotenvy::dotenv;
use thiserror::Error;

use crate::app::{role, user};

pub mod db;
mod tracing;

#[derive(Error, Debug)]
pub enum ConfigurationError {
    #[error("Database Error: {0}")]
    DatabaseError(String),
}
impl From<ConfigurationError> for std::io::Error {
    fn from(e: ConfigurationError) -> Self {
        std::io::Error::new(std::io::ErrorKind::Other, e.to_string())
    }
}
pub fn init_app() {
    dotenv().ok();

    tracing::init_tracing();
}

pub fn services(config: &mut web::ServiceConfig) {
    config.service(
        web::scope("/api")
            .configure(user::config)
            .configure(role::config),
    );
}
