use crate::config::ConfigurationError;

use self::db::Pool;

pub mod db;
pub mod handler;
pub mod member;
pub mod role;
pub mod user;

#[derive(Debug, Clone)]
pub struct Context {
    pub pool: Pool,
}
pub async fn context() -> Result<Context, ConfigurationError> {
    let pool = crate::config::db::pool().await?;
    Ok(Context { pool })
}
