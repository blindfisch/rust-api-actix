use tracing_subscriber::{fmt, fmt::format::FmtSpan, EnvFilter};

pub fn init_tracing() {
    // Configure a custom event formatter
    let format = fmt::format().with_ansi(true).compact();

    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .event_format(format)
        .init();
}
