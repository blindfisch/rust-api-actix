use std::str::FromStr;

use sqlx::migrate::{MigrateError, Migrator};
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};

use crate::app::db::Pool;

use super::ConfigurationError;

impl From<sqlx::Error> for ConfigurationError {
    fn from(err: sqlx::Error) -> Self {
        ConfigurationError::DatabaseError(err.to_string())
    }
}
impl From<MigrateError> for ConfigurationError {
    fn from(err: MigrateError) -> Self {
        ConfigurationError::DatabaseError(err.to_string())
    }
}

pub async fn pool() -> Result<Pool, ConfigurationError> {
    let url = std::env::var("DATABASE_URL").unwrap_or_else(|_| String::from("sqlite::memory:"));

    let options = SqliteConnectOptions::from_str(&url)?
        .create_if_missing(true)
        .pragma("foreign_keys", "ON");

    let pool = SqlitePoolOptions::new()
        .max_connections(5)
        .connect_with(options)
        .await?;

    let path = std::env::var("DATABASE_MIGRATIONS").unwrap_or_else(|_| String::from("./db/migrations"));
    let m = Migrator::new(std::path::Path::new(&path)).await?;
    m.run(&pool).await?;
    Ok(pool)
}
